﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CurrentLocation : MonoBehaviour
{
    static public CurrentLocation instance;
    public Image sceneImage;
    public Location currentLoc;

    void Awake()
    {
        instance = this;
    }
    public static void ChangeLocation(LocationEnum locationE)
    {
        Location newLoc = Location.GetLocationFromEnum(locationE);
        Debug.Log($"New location {newLoc}");
        instance.ChangeLocation(newLoc);
    }

    void ChangeLocation(Location loc)
    {
        currentLoc = loc;
        sceneImage.sprite = loc.sprite;
        Debug.Log($"Changing location to {currentLoc.asEnum} with sprite {loc.sprite}");

    }
}
