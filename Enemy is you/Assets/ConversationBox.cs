﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConversationBox : MonoBehaviour
{
    public List<ReponseButton> responseButtons;
    public Text dialogTextField;
    public Dialog currentDialog;
    public Conversation currentConvo;

    public void SetConversation(Conversation newConversation)
    {
        currentConvo = newConversation;
        ChangeDialog(newConversation.GetDialogForID(0));
    }
    public void ChangeDialog(Dialog dialog)
    {
        currentDialog = dialog;
        dialogTextField.text = dialog.dialogText;
        List<Response> availiableResponses = RemoveUnavaliableResponses(dialog.responses);
        Debug.Log($"Changing to dialog with {availiableResponses.Count} avaliable responses");
        for (int i = responseButtons.Count -1; i >= 0; i--)
        {
            Debug.Log($"Checking for response {i}");
            //If there's more buttons than responses
            if(i >= availiableResponses.Count)
                responseButtons[i].gameObject.SetActive(false);
            else
                responseButtons[i].SetReponse(availiableResponses[i]);
        }
    }

    public void ResponseClicked(int nextDialogID)
    {
        ChangeDialog(currentConvo.GetDialogForID(nextDialogID));
    }
    public List<Response> RemoveUnavaliableResponses(List<Response> responses)
    {
        List<Response> avaliableResponses = responses;
        for (int i = responses.Count-1; i >= 0; i--)
        {
            Response response = responses[i];
            //Check the flags for the response and see if the player has the required set and unset
            if(!response.CheckFlags())
                avaliableResponses.Remove(response);
        }

        return avaliableResponses;
    }
}
