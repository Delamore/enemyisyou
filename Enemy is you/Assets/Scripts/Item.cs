﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum ItemEnum
{
    Gun,
    Briefcase
}
public class Item
{
    public ItemEnum asEnum;
    public Sprite sprite;
    void Awake()
    {
        string path = "Art/Items/" + asEnum;
        sprite = Resources.Load<Sprite>(path);
        Debug.Assert(sprite != null, $"Sprite not found for " + asEnum + " at " + path);
        Debug.Log(sprite);
    }

    Item(ItemEnum itemEnum)
    {
        asEnum = itemEnum;
    }
    public static List<Item> GetAllItems()
    {
        List<Item> items = new List<Item>();
        foreach (ItemEnum itemE in Enum.GetValues(typeof(ItemEnum)))
        {
            Item item = new Item(itemE);
            items.Add(item);
            Debug.Log($"Adding {itemE} to all items");
        }

        return items;
    }
}
