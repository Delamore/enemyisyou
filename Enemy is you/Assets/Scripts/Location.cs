﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LocationEnum
{
    HeadQuarters,
    RebelBase,
    InterrogationRoom
}
public class Location : MonoBehaviour
{
    public LocationEnum asEnum;
    public Sprite sprite;
    public static List<Location> allLocations = new List<Location>();
    void Setup()
    {
        string path = "Art/Locations/" + asEnum;
        sprite = Resources.Load<Sprite>(path);
        Debug.Assert(sprite != null, $"Sprite not found for " + asEnum + " at " + path);
        Debug.Log($" Loading sprite {sprite}");
    }

    public static void LoadAllLocations()
    {
        string path = "Locations/";
        Location[] allPrefabs = Resources.LoadAll<Location>(path);
        foreach (Location loc in allPrefabs)
        {
            Location spawnedLoc = GameObject.Instantiate(loc);
            Debug.Log($"Instantated copy of {loc}");
            spawnedLoc.Setup();
            loc.name = loc.asEnum.ToString();
            CheckIfLocationIsDuplicate(spawnedLoc.asEnum);
            allLocations.Add(spawnedLoc);
            Debug.Log($"Spawned location {loc.asEnum}");
        }
    }

    static void CheckIfLocationIsDuplicate(LocationEnum locE)
    {
        foreach (Location loc in allLocations)
        {
            if (loc.asEnum == locE)
            {
                Debug.Log(allLocations.Count);
                foreach (Location a in allLocations)
                {
                    Debug.Log(a);
                }

                throw new Exception($"{locE} exists as duplicate in Locations folder");
            }
        }
    }

    public static Location GetLocationFromEnum(LocationEnum locE)
    {
        foreach (Location loc in allLocations)
        {
            if (loc.asEnum == locE)
                return loc;
        }
        throw new Exception($"Location not found for {locE}");
    }
}
