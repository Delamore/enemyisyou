﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conversation : MonoBehaviour
{
    public Conversation instance;
    private List<Dialog> dialogs;
    private int priority = 0;
    private bool autoStart = false;
    public Conversation(List<Dialog> dialogs, int priority, bool autoStart)
    {
        this.dialogs = dialogs;
        this.priority = priority;
        this.autoStart = autoStart;
    }

    public Dialog GetDialogForID(int nextDialogID)
    {
        foreach (Dialog dia in dialogs)
        {
            if (dia.IDWithinConversation == nextDialogID)
                return dia;
        }
        throw new Exception($"No dialog found with id {nextDialogID}");
    }

}

public struct Dialog
{
    public string dialogText;
    public  int IDWithinConversation;
    public List<Response> responses;
    public List<FlagEnum> flagsToSet;
    public List<FlagEnum> flagsToUnset;
    public Dialog(
        string text, 
        int ID,
        List<Response> responses, 
        List<FlagEnum> flagsToSet, 
        List<FlagEnum> flagsToUnset)
    {
        this.dialogText = text;
        this.responses = responses;
        this.flagsToSet = flagsToSet;
        this.flagsToUnset = flagsToUnset;
        this.IDWithinConversation = ID;
    }
}

public struct Response
{
    public int nextDialogID;
    public string responseText;
    public List<FlagEnum> requiredSetFlags;
    public List<FlagEnum> requiredUnsetFlags;
    public bool endOfConversation;
    public Response(
        string responseText,
        int nextDialogID,
        List<FlagEnum> requiredSetFlags, 
        List<FlagEnum> requiredUnsetFlags,
        bool endOfConversation)
    {
        this.nextDialogID = nextDialogID;
        this.responseText = responseText;
        this.requiredSetFlags = requiredSetFlags;
        this.requiredUnsetFlags = requiredUnsetFlags;
        this.endOfConversation = endOfConversation;
    }

    public bool CheckFlags()
    {
        return true;
    }
}