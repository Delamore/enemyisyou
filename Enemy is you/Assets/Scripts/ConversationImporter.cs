﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConversationImporter
{
    static void ImportConversation()
    {
        //For each dialog within the xml
        List<Dialog> importedDialogs = new List<Dialog>();
        ImportOneDialog();
    }

    static Dialog ImportOneDialog()
    {
        //Import the text of the dialog
        string dialogText = "";

        //Import the dialog ID within the conversation
        int ID = 0;
        //Import the flags to set
        List<FlagEnum> flagsToSet = new List<FlagEnum>();

        //Import the flags to unset
        List<FlagEnum> flagsToUnset = new List<FlagEnum>();

        //Import the responses
        List<Response> responses = new List<Response>();

        for (int i = 0; i > 0; i--)
        {
            //Import response text
            string responseText = "";

            //Import ID of next dialog
            int nextDialogID = 0;

            //Import required flags to set
            List<FlagEnum> requiredSetFlags = new List<FlagEnum>();

            //Import flags required unset
            List<FlagEnum> requiredUnsetFlags = new List<FlagEnum>();

            Response importedReponse = new Response(responseText, nextDialogID, requiredSetFlags, requiredUnsetFlags, false);
            responses.Add(importedReponse);
        }

        Dialog dialog = new Dialog(dialogText, ID, responses, flagsToSet, flagsToUnset);
        return dialog;
    }


    #region DEBUG

    static public void MakeFakeConversation()
    {
        List<Dialog> fakeDialog = new List<Dialog>();
        Debug.Log($"Making fake dialog 1");
        fakeDialog.Add(MakeFakeDialog1());
        Debug.Log($"Making fake dialog 2");
        fakeDialog.Add(MakeFakeDialog2());
        Debug.Log($"Making fake dialog 3");
        fakeDialog.Add(MakeFakeDialog3());
        Debug.Log($"Making fake dialog 4");
        fakeDialog.Add(MakeFakeDialog4());
        int priority = 1;
        Debug.Log($"Making fake Conversation");
        Conversation convo = new Conversation(fakeDialog, priority, true);
        Debug.Log("Finding conversation Box");
        ConversationBox convoBox = GameObject.FindObjectOfType<ConversationBox>();
        Debug.Log("Setting current Conversation to fake convo");
        convoBox.SetConversation(convo);
    }
    static Dialog MakeFakeDialog1()
    {
        //Import the text of the dialog
        string dialogText = "This is a test dialog";

        //Import the dialog ID within the conversation
        int ID = 0;
        //Import the flags to set
        List<FlagEnum> flagsToSet = new List<FlagEnum>();

        //Import the flags to unset
        List<FlagEnum> flagsToUnset = new List<FlagEnum>();

        //Import the responses
        List<Response> responses = new List<Response>();

        for (int i = 2; i > 0; i--)
        {
            if (i == 1)
            {
                //Import response text
                string responseText = "Response 1";

                //Import ID of next dialog
                int nextDialogID = 1;

                //Import required flags to set
                List<FlagEnum> requiredSetFlags = new List<FlagEnum>();

                //Import flags required unset
                List<FlagEnum> requiredUnsetFlags = new List<FlagEnum>();

                Response importedReponse = new Response(responseText, nextDialogID, requiredSetFlags, requiredUnsetFlags, false);
                responses.Add(importedReponse);
            }
            else
            {
                //Import response text
                string responseText = "Response 2";

                //Import ID of next dialog
                int nextDialogID = 2;

                //Import required flags to set
                List<FlagEnum> requiredSetFlags = new List<FlagEnum>();

                //Import flags required unset
                List<FlagEnum> requiredUnsetFlags = new List<FlagEnum>();

                Response importedReponse = new Response(responseText, nextDialogID, requiredSetFlags, requiredUnsetFlags, false);
                responses.Add(importedReponse);
            }
        }

        Dialog dialog = new Dialog(dialogText, ID, responses, flagsToSet, flagsToUnset);
        return dialog;
    }

    static Dialog MakeFakeDialog2()
    {
        //Import the text of the dialog
        string dialogText = "This is a test dialog in response to response 1 of test dialog 1";

        //Import the dialog ID within the conversation
        int ID = 1;
        //Import the flags to set
        List<FlagEnum> flagsToSet = new List<FlagEnum>();

        //Import the flags to unset
        List<FlagEnum> flagsToUnset = new List<FlagEnum>();

        //Import the responses
        List<Response> responses = new List<Response>();

        for (int i = 1; i > 0; i--)
        {

            //Import response text
            string responseText = "Response 1";

            //Import ID of next dialog
            int nextDialogID = 3;

            //Import required flags to set
            List<FlagEnum> requiredSetFlags = new List<FlagEnum>();

            //Import flags required unset
            List<FlagEnum> requiredUnsetFlags = new List<FlagEnum>();

            Response importedReponse = new Response(responseText, nextDialogID, requiredSetFlags, requiredUnsetFlags, false);
            responses.Add(importedReponse);
        }

        Dialog dialog = new Dialog(dialogText, ID, responses, flagsToSet, flagsToUnset);
        return dialog;
    }
    static Dialog MakeFakeDialog3()
    {
        //Import the text of the dialog
        string dialogText = "This is a test dialog in response to response 2 of test dialog 1";

        //Import the dialog ID within the conversation
        int ID = 2;
        //Import the flags to set
        List<FlagEnum> flagsToSet = new List<FlagEnum>();

        //Import the flags to unset
        List<FlagEnum> flagsToUnset = new List<FlagEnum>();

        //Import the responses
        List<Response> responses = new List<Response>();

        for (int i = 1; i > 0; i--)
        {

            //Import response text
            string responseText = "Response 1";

            //Import ID of next dialog
            int nextDialogID = 3;

            //Import required flags to set
            List<FlagEnum> requiredSetFlags = new List<FlagEnum>();

            //Import flags required unset
            List<FlagEnum> requiredUnsetFlags = new List<FlagEnum>();

            Response importedReponse = new Response(responseText, nextDialogID, requiredSetFlags, requiredUnsetFlags, true);
            responses.Add(importedReponse);
        }

        Dialog dialog = new Dialog(dialogText, ID, responses, flagsToSet, flagsToUnset);
        return dialog;
    }

    static Dialog MakeFakeDialog4()
    {
        //Import the text of the dialog
        string dialogText = "This is the final test dialog";

        //Import the dialog ID within the conversation
        int ID = 3;
        //Import the flags to set
        List<FlagEnum> flagsToSet = new List<FlagEnum>();

        //Import the flags to unset
        List<FlagEnum> flagsToUnset = new List<FlagEnum>();

        //Import the responses
        List<Response> responses = new List<Response>();

        for (int i = 1; i > 0; i--)
        {

            //Import response text
            string responseText = "end conversation";

            //Import ID of next dialog
            int nextDialogID = 3;

            //Import required flags to set
            List<FlagEnum> requiredSetFlags = new List<FlagEnum>();

            //Import flags required unset
            List<FlagEnum> requiredUnsetFlags = new List<FlagEnum>();

            Response importedReponse = new Response(responseText, nextDialogID, requiredSetFlags, requiredUnsetFlags, false);
            responses.Add(importedReponse);
        }

        Dialog dialog = new Dialog(dialogText, ID, responses, flagsToSet, flagsToUnset);
        return dialog;
    }
    #endregion

}
