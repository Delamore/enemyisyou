﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    void Start()
    {
        Location.LoadAllLocations();
        CurrentLocation.ChangeLocation(0);
        ConversationImporter.MakeFakeConversation();
    }
}
