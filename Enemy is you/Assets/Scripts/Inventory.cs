﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public static Inventory instance;
    public List<Button> buttons;
    List<Item> itemsInInventory = new List<Item>();
    private List<Item> allItems = new List<Item>();

    void Awake()
    {
        allItems = Item.GetAllItems();
        instance = this;
    }
}

