﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReponseButton : MonoBehaviour
{
    private Button button;
    private Text text;
    private ConversationBox convoBox;
    private int nextDialogID;
    void Awake()
    {
        button = GetComponent<Button>();
        text = transform.GetChild(0).gameObject.GetComponent<Text>();
        convoBox = GameObject.FindObjectOfType<ConversationBox>();
        button.onClick.AddListener(OnClick);
    }
    public void SetReponse(Response response)
    {
        text.text = response.responseText;
        nextDialogID = response.nextDialogID;
    }

    public void OnClick()
    {
        Debug.Log($"Response clicked, moving to dialog ID {nextDialogID}");
        convoBox.ResponseClicked(nextDialogID);
    }
}
